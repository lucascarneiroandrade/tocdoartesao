from django.db import models
import datetime


class Pessoa(models.Model):
    nome = models.CharField(max_length=30, null=True, blank=True)
    sobrenome = models.CharField(max_length=30, null=True, blank=True)
    datanascimento = models.DateField(default=datetime.date.today)
    email = models.CharField(max_length=50, null=True, blank=True)
    telefone = models.CharField(max_length=9, null=True, blank=True)
    endereco = models.CharField(max_length=100, null=True, blank=True)
    login = models.CharField(max_length=50, null=True, blank=True)
    senha = models.CharField(max_length=50, null=True, blank=True)
