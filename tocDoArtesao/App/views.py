from django.shortcuts import render, redirect, get_object_or_404
from App.models import Pessoa
from App.forms import PessoaForm
from django.views import View


class MyView(View):
    def create(request):
        form = PessoaForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list')
        return render(request, 'create.html', {'pessoa': form})

    def update(request, id):
        pessoa = get_object_or_404(Pessoa, pk=id)
        form = PessoaForm(request.POST or None, instance=pessoa)
        if form.is_valid():
            form.save()
            return redirect('list')
        return render(request, 'update.html', {'form': form})

    def delete(request, id):
        pessoa = get_object_or_404(Pessoa, pk=id)
        if request.method == 'POST':
            pessoa.delete()
            return redirect('list')
        return render(request, 'delete.html', {'pessoa': pessoa})

    def list(request):
        pessoa = Pessoa.objects.all()
        return render(request, 'list.html', {'pessoa': pessoa})
