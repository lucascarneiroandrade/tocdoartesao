from django.urls import path
from App.views import MyView

urlpatterns = [
    path('', MyView.list, name='list'),
    path('create/', MyView.create, name='create'),
    path('update/<int:id>/', MyView.update, name='update'),
    path('delete/<int:id>/', MyView.delete, name='delete'),
]
